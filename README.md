#wheezy.o2p(Oauth2Provider)


wheezy.o2p is a python package written in pure python code. It's a lightweight OAuth2 Provider for things like Authorization, Token and Resource Provider.

## `wheezy.o2p` depends on `wheezy.http`
[wheezy.http](https://bitbucket.org/akorn/wheezy.http) is a lightweight http library which is used for things like request and response.

Wheezy is a lightweight framework, which is best as a REST framework.
That is the main reason I'm trying to build an Oauth 2 Provider for Wheezy.

Before you start implementing you need to have the basic knowledge of Oauth2.

[OAuth: A Tale of Two Servers](https://www.youtube.com/watch?v=tFYrq3d54Dc)

##First: Authorization, Storing and Redirecting

1. `get_authorization_code_from_uri` the function which should be called when the user requests.
2. `get_authorization_code_from_uri` -> `get_authorization_code` to validate everything and 
3. `get_authorization_code` -> `generate_authorization_code` for `code`
4. `get_authorization_code` -> `persist_authorization_code` for `code` should be stored only for a short time.
5. `get_authorization_code` -> Redirects you to `redirect_uri` with `code` parameter.

##Second: Access Token

1. Client now has the `code`.
2. `POST` method should be used to get the token.
3. `get_token_from_post_data` with data from the parameters as `iterkeys`.
4. `get_token_from_post_data` passes data to `get_token`
    + code
    + grant_type
    + client_id
    + client_secret
    + redirect_uri
4. `code` is discarded.
5. `access_token`, `token_type`, `expires_in`, `refresh_token` are generated and stored.

## Third: Refresh Token

1. if `grant_type` is `refresh_token` in data passed to `get_token_from_post_data`.
2. `get_token_from_post_data` passes the message to `refresh_token`