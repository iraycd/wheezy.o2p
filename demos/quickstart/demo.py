import json

from wheezy.http import WSGIApplication
from wheezy.routing import url
from wheezy.web.handlers import BaseHandler
from wheezy.web.middleware import bootstrap_defaults
from wheezy.web.middleware import path_routing_middleware_factory

from o2p import AuthorizationProvider


class DemoAuthorizationProvider(AuthorizationProvider):

    def validate_scope(self, client_id, scope):
        return True

    def validate_client_id(self, client_id):
        return True

    def validate_client_secret(self, client_id, client_secret):
        return True

    def validate_redirect_uri(self, client_id, redirect_uri):
        return True

    def validate_access(self):
        return True

    def persist_authorization_code(self, client_id, code, scope):
        print(" persist_authorization_code")

    def persist_token_information(self, client_id, scope, access_token,
                                  token_type, expires_in, refresh_token,
                                  data):
        print(" persist_token_information")

    def discard_authorization_code(self, client_id, code):
        print(" discard_authorization_code")

    def discard_refresh_token(self, client_id, refresh_token):
        print(" discard_refresh_token")

    def from_refresh_token(self, client_id, refresh_token, scope):
        return "Hello"

    def from_authorization_code(self, client_id, code, scope):
        return "Hello"


class AuthorizationCode(BaseHandler):

    def get(self):
        provider = DemoAuthorizationProvider()
        response = provider.get_authorization_code_from_uri(self.request)
        return response


class Token(BaseHandler):

    def post(self):
        provider = DemoAuthorizationProvider()
        response = provider.get_token_from_post_data(self.request)
        return response

all_urls = [
    url('v1/oauth2/auth', AuthorizationCode, name='auth'),
    url('v1/oauth2/token', Token, name='token')
]


options = {}


main = WSGIApplication(
    middleware=[
        bootstrap_defaults(url_mapping=all_urls),
        path_routing_middleware_factory
    ],
    options=options
)
