#!/usr/bin/env python

import os

try:
    from setuptools import setup
except:
    from distutils.core import setup


extra = {}
try:
    from Cython.Build import cythonize
    path = os.path.join('src', 'wheezy', 'o2p')
    extra['ext_modules'] = cythonize(
        [os.path.join(path, '*.py')],
        quiet=True)
except ImportError:
    pass

README = open(os.path.join(os.path.dirname(__file__), 'README.md')).read()

setup(
    name='wheezy.o2p',
    version='0.1',
    description='A lightweight oauth2 provider library',
    long_description=README,
    url='https://bitbucket.org/iraycd/wheezy.o2p',

    author='Ray Ch',
    author_email='ray at ninjaas.com',

    license='MIT',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.4',
        'Programming Language :: Python :: 2.5',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.2',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: Implementation :: CPython',
        'Programming Language :: Python :: Implementation :: PyPy',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Topic :: Internet :: WWW/HTTP :: Site Management',
        'Topic :: Internet :: WWW/HTTP :: WSGI :: Application',
        'Topic :: Internet :: WWW/HTTP :: WSGI :: Middleware',
        'Topic :: Internet :: WWW/HTTP :: WSGI :: Server'

    ],
    keywords='oauth2 auth provider',
    packages=['wheezy', 'wheezy.o2p'],
    package_dir={'': 'src'},
    namespace_packages=['wheezy'],

    zip_safe=False,
    install_requires = [
        'wheezy.core>=0.1.131',
        'wheezy.http>=0.1.314',
    ],

    platforms='any',
    **extra
)